﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class VirtualButtonScript : MonoBehaviour
{
    public GameObject sphereGo; 
    public  GameObject cubeGo;
    public GameObject button;
   // VirtualButtonBehaviour[] vbs;
    // Start is called before the first frame update
    void Start()
    {
        button.GetComponent<VirtualButtonBehaviour>().RegisterOnButtonPressed(OnButtonPressed);
        button.GetComponent<VirtualButtonBehaviour>().RegisterOnButtonReleased(OnButtonReleased);


        //vbBtnObj.GetComponent<VirtualButtonBehaviour>().RegisterEventHandler(this);
       // vbs = GetComponentsInChildren<VirtualButtonBehaviour>();
       // for (int i = 0; i < vbs.Length; ++i)
      //  {
      //      vbs[i].RegisterOnButtonPressed(OnButtonPressed);
      //      vbs[i].RegisterOnButtonReleased(OnButtonReleased);
      //  }
        sphereGo.SetActive(true);
        cubeGo.SetActive(false);
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        sphereGo.SetActive(false);
        cubeGo.SetActive(true);
    }

    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        cubeGo.SetActive(false);
        sphereGo.SetActive(true);
    }
}