﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Vuforia;

public class ARBCard : MonoBehaviour
{
    public GameObject developergo, trainergo;
    // Start is called before the first frame update
    void Start()
    {
        VirtualButtonBehaviour[] vrb = GetComponentsInChildren<VirtualButtonBehaviour>();
        for (int i = 0; i < vrb.Length; i++)
        {
            vrb[i].RegisterOnButtonPressed(OnButtonPressed);
            vrb[i].RegisterOnButtonReleased(OnButtonReleased);
        }
        developergo.SetActive(false);
        trainergo.SetActive(false);
      //  travellergo.SetActive(false);
    }

    public void OnButtonPressed(VirtualButtonBehaviour vb)
    {
        if (vb.VirtualButtonName == "Vbtn")
        {
            UnityEngine.Debug.Log("Developer Button pressed");
            developergo.SetActive(true);
            trainergo.SetActive(false);
          //  travellergo.SetActive(false);
        }
        else if (vb.VirtualButtonName == "Vbtn1")
        {
            UnityEngine.Debug.Log("Trainer Button pressed");
            developergo.SetActive(false);
            trainergo.SetActive(true);
          //  travellergo.SetActive(false);
        }
        
        {
            throw new UnityException(vb.VirtualButtonName + "Virtual Button not supported");
        }
    }
    public void OnButtonReleased(VirtualButtonBehaviour vb)
    {
        UnityEngine.Debug.Log(vb.VirtualButtonName + " released");
    }
}